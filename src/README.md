# charm-neutron-api-genericswitch

This charm adds subordinate support for [Networking Generic Switch](https://docs.openstack.org/networking-generic-switch/latest/)
to [charm-neutron-api](https://git.openstack.org/cgit/openstack/charm-neutron-api/).

## Restrictions
This charm currently only supports deployment with OpenStack Newton.
